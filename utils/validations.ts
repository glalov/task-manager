//Email Validates

const validateEmail = (email: string) =>
  email.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i);

export const isInvalidEmail = (email: string) => {
  if (email === "") return false;

  return validateEmail(email) ? false : true;
};

export const emailErrorText = "Please enter a valid email address";

//Password Validates

export const isInvalidPassword = (password: string) => {
  if (password === "") return false;

  return password.length >= 6 ? false : true;
};

export const passwordErrorText = "Password must be at least 6 characters";
