import React from "react";
import { Modal } from "@nextui-org/react";

export type ModalParams = {
  content: React.ReactNode;
};

type UnifiedModalProps = {
  isOpen: boolean;
  onOpenChange: (open: boolean) => void;
} & ModalParams;

const UnifiedModal = ({ isOpen, onOpenChange, content }: UnifiedModalProps) => {
  return (
    <>
      <Modal isOpen={isOpen} onOpenChange={onOpenChange} placement="top-center">
        {content}
      </Modal>
    </>
  );
};

export default UnifiedModal;
