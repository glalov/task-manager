"use client";

import {
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Input,
} from "@nextui-org/react";
import { MailIcon } from "./MailIcon";
import { LockIcon } from "./LockIcon";
import { useState } from "react";
import signUp from "@/firebase/auth/signup";
import { createUser } from "@/firebase/actions/user.action";
import { UserCredential } from "firebase/auth";
import {
  emailErrorText,
  isInvalidEmail,
  isInvalidPassword,
  passwordErrorText,
} from "@/utils/validations";
import { useToast } from "@/components/ui/use-toast";

const SignUpModalContent = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorText, setErrorText] = useState("");
  const { toast } = useToast();

  const handleSubmit = async (event: any, onClose: () => void) => {
    event.preventDefault();
    setIsLoading(true);

    // Check if inputs are valid
    if (isInvalidEmail(email) || isInvalidPassword(password)) {
      setIsLoading(false);
      return;
    }

    let userCredential: UserCredential | null = null;

    try {
      // Create user in Auth
      userCredential = await signUp(email, password);
    } catch (error: any) {
      const errorCode = error.code;
      //const errorMessage = error.message;

      if (errorCode === "auth/email-already-in-use") {
        setErrorText("That email address is already in use!");
      } else if (errorCode === "auth/invalid-email") {
        setErrorText("That email address is invalid!");
      } else if (errorCode === "auth/weak-password") {
        setErrorText("That password is too weak!");
      } else {
        setErrorText("Something went wrong!");
      }

      setIsLoading(false);
      return;
    }

    try {
      // Create user in the database
      await createUser(userCredential.user.uid, email);
    } catch (error) {
      console.log(error);

      setErrorText("Something went wrong!");
      setIsLoading(false);
      return;
    }

    setIsLoading(false);
    setErrorText("");
    onClose();
    toast({
      title: "Success",
      description: "Successfully created your account!",
    });
  };

  return (
    <ModalContent>
      {(onClose) => (
        <form onSubmit={(e) => handleSubmit(e, onClose)}>
          <ModalHeader className="flex flex-col gap-1">Sign Up</ModalHeader>
          <ModalBody>
            <Input
              onChange={(e) => setEmail(e.target.value)}
              autoFocus
              endContent={
                <MailIcon className="text-2xl text-default-400 pointer-events-none flex-shrink-0" />
              }
              label="Email"
              placeholder="Enter your email"
              type="email"
              variant="bordered"
              disabled={isLoading}
              isRequired
              isInvalid={isInvalidEmail(email)}
              color={isInvalidEmail(email) ? "danger" : "default"}
              errorMessage={isInvalidEmail(email) && emailErrorText}
            />
            <Input
              onChange={(e) => setPassword(e.target.value)}
              endContent={
                <LockIcon className="text-2xl text-default-400 pointer-events-none flex-shrink-0" />
              }
              label="Password"
              placeholder="Enter your password"
              type="password"
              variant="bordered"
              disabled={isLoading}
              isRequired
              isInvalid={isInvalidPassword(password)}
              color={isInvalidPassword(password) ? "danger" : "default"}
              errorMessage={isInvalidPassword(password) && passwordErrorText}
            />
          </ModalBody>
          <div className="text-center text-danger-500">{errorText}</div>
          <ModalFooter>
            <Button color="danger" variant="flat" onPress={onClose}>
              Close
            </Button>
            <Button disabled={isLoading} color="primary" type="submit">
              Sign Up
            </Button>
          </ModalFooter>
        </form>
      )}
    </ModalContent>
  );
};

export default SignUpModalContent;
