import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "@/firebase/config";

export default async function logIn(email: string, password: string) {
  return await signInWithEmailAndPassword(auth, email, password);
}
