import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "@/firebase/config";

export default async function signUp(email: string, password: string) {
  return await createUserWithEmailAndPassword(auth, email, password);
}
