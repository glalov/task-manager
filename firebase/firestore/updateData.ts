import { doc, setDoc } from "firebase/firestore";
import { db } from "../config";

export async function updateData(collection: string, id: string, data: any) {
  return await setDoc(doc(db, collection, id), data, {
    merge: true,
  });
}
