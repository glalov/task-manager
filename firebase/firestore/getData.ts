import { DocumentSnapshot, doc, getDoc, onSnapshot } from "firebase/firestore";
import { db } from "../config";

export async function getDocument(collection: string, id: string) {
  return await getDoc(doc(db, collection, id));
}

export function setListener(
  collection: string,
  id: string,
  callback: (snapshot: DocumentSnapshot) => void
) {
  return onSnapshot(doc(db, collection, id), callback);
}
