export type UserData = {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  username: string;
  image: string;
  createdAt: Date;
  isOnboarded: boolean;
};
