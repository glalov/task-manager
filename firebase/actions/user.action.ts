import { DocumentSnapshot } from "firebase/firestore";
import { getDocument, setListener } from "../firestore/getData";
import { updateData } from "../firestore/updateData";
import { UserData } from "../models";

export const updateUser = async (id: string, data: UserData) => {
  return await updateData("users", id, data);
};

export const createUser = async (id: string, email: string) => {
  return await updateUser(id, {
    id,
    email,
    firstName: "",
    lastName: "",
    username: "",
    image: "",
    createdAt: new Date(),
    isOnboarded: false,
  });
};

export const getUserData = async (id: string) => {
  const result = await getDocument("users", id);

  if (result.exists()) {
    return result.data() as UserData;
  }

  return null;
};

export const setUserDataListener = (
  id: string,
  callback: (data: DocumentSnapshot) => void
) => {
  return setListener("users", id, callback);
};
