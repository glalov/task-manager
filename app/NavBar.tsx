"use client";

import React from "react";
import {
  Navbar,
  NavbarBrand,
  NavbarMenuToggle,
  NavbarMenuItem,
  NavbarMenu,
  NavbarContent,
  NavbarItem,
  Link,
  Button,
} from "@nextui-org/react";
import { appName } from "@/utils/constants";
import { useModalContext } from "@/context/ModalContext";
import SignUpModalContent from "@/components/modal/SignUpModalContent";
import { useAuthContext } from "@/context/AuthContext";
import signout from "@/firebase/auth/signout";
import LogInModalContent from "@/components/modal/LogInModalContent";

const NavBar = () => {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const { openModal } = useModalContext();
  const { userData } = useAuthContext();

  const onSignUp = () => {
    openModal({ content: <SignUpModalContent /> });
  };

  const onLogIn = () => {
    openModal({ content: <LogInModalContent /> });
  };

  const onSignOut = async () => {
    try {
      await signout();
    } catch (error) {
      // TODO Log error
      console.log(error);
    }
  };

  const smMenuItems = [
    "Profile",
    "Dashboard",
    "Tasks",
    "Groups",
    "Calendar",
    "Settings",
    "Log Out",
  ];

  return (
    <Navbar
      isBordered
      isMenuOpen={isMenuOpen}
      onMenuOpenChange={setIsMenuOpen}
      maxWidth="full"
    >
      <NavbarContent className="sm:hidden" justify="start">
        <NavbarMenuToggle
          aria-label={isMenuOpen ? "Close menu" : "Open menu"}
        />
      </NavbarContent>

      <NavbarContent className="sm:hidden pr-3" justify="center">
        <NavbarBrand>
          <p className="font-bold text-inherit">{appName}</p>
        </NavbarBrand>
      </NavbarContent>

      <NavbarContent className="hidden sm:flex gap-4" justify="center">
        <NavbarBrand>
          <p className="font-bold text-inherit">{appName}</p>
        </NavbarBrand>
        <NavbarItem>
          <Link color="foreground" href="#">
            Tasks
          </Link>
        </NavbarItem>
        <NavbarItem>
          <Link color="foreground" href="#">
            Groups
          </Link>
        </NavbarItem>
      </NavbarContent>

      <NavbarContent justify="end">
        {!userData ? (
          <>
            <NavbarItem>
              <Button onClick={() => onLogIn()} color="warning" variant="flat">
                Login
              </Button>
            </NavbarItem>
            <NavbarItem>
              <Button onClick={() => onSignUp()} color="warning" variant="flat">
                Sign Up
              </Button>
            </NavbarItem>
          </>
        ) : (
          <NavbarItem>
            <Button onClick={() => onSignOut()} color="warning" variant="flat">
              Log-out
            </Button>
          </NavbarItem>
        )}
      </NavbarContent>

      <NavbarMenu>
        {smMenuItems.map((item, index) => (
          <NavbarMenuItem key={`${item}-${index}`}>
            <Link
              className="w-full"
              color={index === smMenuItems.length - 1 ? "danger" : "foreground"}
              href="#"
              size="lg"
            >
              {item}
            </Link>
          </NavbarMenuItem>
        ))}
      </NavbarMenu>
    </Navbar>
  );
};

export default NavBar;
