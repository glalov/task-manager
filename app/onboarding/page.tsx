"use client";

import { useToast } from "@/components/ui/use-toast";
import { useAuthContext } from "@/context/AuthContext";
import { Image, Input } from "@nextui-org/react";
import { redirect } from "next/navigation";
import { ChangeEvent, FormEvent, useState } from "react";
import { useUploadThing } from "@/utils/uploadthing";
import { isBase64Image } from "@/utils/utils";

const OnboardingPage = () => {
  // Check if user is logged in and isOnboarded is false, if not redirect to home
  const { userData } = useAuthContext();

  if (!userData || userData.isOnboarded) {
    // redirect to home
    redirect("/");
  }

  const [isLoading, setIsLoading] = useState(false);
  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    username: "",
    image: "",
  });
  const [errorText, setErrorText] = useState("");
  const { toast } = useToast();

  const { startUpload } = useUploadThing("imageUploader");
  const [files, setFiles] = useState<File[]>([]);

  const setField = (e: ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  const setImage = (value: string) => {
    setForm({
      ...form,
      ["image"]: value,
    });
  };

  const handleImage = (
    e: ChangeEvent<HTMLInputElement>,
    fieldChange: (value: string) => void
  ) => {
    e.preventDefault();

    const fileReader = new FileReader();

    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      setFiles(Array.from(e.target.files));

      if (!file.type.includes("image")) return;

      fileReader.onload = async (event) => {
        const imageDataUrl = event.target?.result?.toString() || "";
        fieldChange(imageDataUrl);
      };

      fileReader.readAsDataURL(file);
    }
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setIsLoading(true);

    // Check if inputs are valid
    if (false) {
      setIsLoading(false);
      return;
    }

    // Check if username is already taken

    // Upload profile image
    const blob = form.image;
    const hasImageChanged = isBase64Image(blob);
    if (hasImageChanged) {
      const imgRes = await startUpload(files);

      if (imgRes && imgRes[0].url) {
        form.image = imgRes[0].url;
      }
    }

    setIsLoading(false);
    setErrorText("");
    toast({
      title: "Success",
      description: "Successfully updated your account!",
    });
  };

  return (
    <div>
      <div>Onboarding page</div>

      <form onSubmit={handleSubmit}>
        {form.image ? (
          <Image
            src={form.image}
            alt="profile_icon"
            width={96}
            height={96}
            className="rounded-full object-contain"
          />
        ) : (
          <Image
            src="/assets/profile.svg"
            alt="profile_icon"
            width={24}
            height={24}
            className="object-contain"
          />
        )}

        <Input
          onChange={(e) => handleImage(e, setImage)}
          label="image"
          name="image"
          placeholder="Add profile photo"
          type="file"
          accept="image/*"
          disabled={isLoading}
        />
        <Input
          onChange={setField}
          autoFocus
          label="firstName"
          name="firstName"
          placeholder="Enter your First Name"
          type="text"
          variant="bordered"
          disabled={isLoading}
          isRequired
        />
        <Input
          onChange={setField}
          autoFocus
          label="lastName"
          name="lastName"
          placeholder="Enter your Last Name"
          type="text"
          variant="bordered"
          disabled={isLoading}
          isRequired
        />
        <Input
          onChange={setField}
          autoFocus
          label="username"
          name="username"
          placeholder="Enter username"
          type="text"
          variant="bordered"
          disabled={isLoading}
          isRequired
        />
      </form>
    </div>
  );
};

export default OnboardingPage;
