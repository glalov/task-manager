"use client";
import React from "react";
import { useAuthContext } from "@/context/AuthContext";
import { redirect } from "next/navigation";

export default function Home() {
  const { userData } = useAuthContext();

  // If user is not onboarded, redirect to onboarding page
  if (userData && !userData.isOnboarded) {
    // redirect to onboarding
    redirect("/onboarding");
  }

  return <h1>{userData ? userData.email : "None"}</h1>;
}
