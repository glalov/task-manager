"use client";

import UnifiedModal, { ModalParams } from "@/components/modal/UnifiedModal";
import { useDisclosure } from "@nextui-org/react";
import { createContext, useContext, useEffect, useState } from "react";

type ModalContextType = {
  openModal: (params: ModalParams) => void;
};

const defaultModalContext = {
  openModal: () => {},
};

const defaultModalParams = {
  content: null,
};

export const ModalContext =
  createContext<ModalContextType>(defaultModalContext);

export const useModalContext = () => useContext(ModalContext);

type ModalContextProviderProps = {
  children: React.ReactNode;
};

export const ModalContextProvider = ({
  children,
}: ModalContextProviderProps) => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();

  const [modalParams, setModalParams] =
    useState<ModalParams>(defaultModalParams);

  const openModal = (params: ModalParams) => {
    setModalParams(params);
    onOpen();
  };

  useEffect(() => {
    if (!isOpen) {
      setModalParams(defaultModalParams);
    }
  }, [isOpen]);

  return (
    <ModalContext.Provider value={{ openModal }}>
      {children}
      <UnifiedModal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        {...modalParams}
      />
    </ModalContext.Provider>
  );
};
