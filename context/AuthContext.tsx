"use client";

import { setUserDataListener } from "@/firebase/actions/user.action";
import { auth } from "@/firebase/config";
import { UserData } from "@/firebase/models";
import { User, onAuthStateChanged } from "firebase/auth";
import { createContext, useContext, useEffect, useState } from "react";

type AuthContextType = {
  userData: UserData | null;
};

const defaultAuthContext = {
  userData: null,
};

export const AuthContext = createContext<AuthContextType>(defaultAuthContext);

export const useAuthContext = () => useContext(AuthContext);

type AuthContextProviderProps = {
  children: React.ReactNode;
};

export const AuthContextProvider = ({ children }: AuthContextProviderProps) => {
  const [userData, setUserData] = useState<UserData | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        const unsub = setUserDataListener(user.uid, (doc) => {
          if (doc.exists()) {
            setUserData(doc.data() as UserData);
          }
        });
      } else {
        setUserData(null);
      }
      setLoading(false);
    });

    return () => unsubscribe();
  }, []);

  return (
    <AuthContext.Provider value={{ userData }}>
      {loading ? <div>Loading...</div> : children}
    </AuthContext.Provider>
  );
};
