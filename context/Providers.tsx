"use client";

import { NextUIProvider } from "@nextui-org/react";
import { useRouter } from "next/navigation";
import { AuthContextProvider } from "./AuthContext";
import { ModalContextProvider } from "./ModalContext";

type ProvidersProps = {
  children: React.ReactNode;
};

const Providers = ({ children }: ProvidersProps) => {
  const router = useRouter();
  return (
    <NextUIProvider navigate={router.push}>
      <AuthContextProvider>
        <ModalContextProvider>{children}</ModalContextProvider>
      </AuthContextProvider>
    </NextUIProvider>
  );
};

export default Providers;
